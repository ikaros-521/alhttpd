#[allow(unused_imports)]
use std::sync::atomic::*;
/*
** Double any double-quote characters in a string.
** 将字符串中的双引号加倍
*/
pub fn escape(z: &str) -> String {
    let mut res = String::new();
    for ch in z.chars() {
        if ch == '"' {
            res.push(ch);
            res.push(ch);
        } else {
            res.push(ch);
        }
    }
    res
}
pub struct CgienvStruct {
    z_env_name: &str,
    pz_env_value: i32,
}
impl CgienvStruct {
    pub fn new(s: &str, p: i32) -> Self {
        CgienvStruct {
            z_env_name: s,
            pz_env_value: p,
        }
    }
}
static CGIENV: [CgienvStruct; 10] = [
    CgienvStruct::new( "CONTENT_LENGTH",              &zContentLength ), /* Must be first for SCGI */
    CgienvStruct::new( "AUTH_TYPE",                   &zAuthType ),
    CgienvStruct::new( "AUTH_CONTENT",                &zAuthArg ),
    CgienvStruct::new( "CONTENT_TYPE",                &zContentType ),
    CgienvStruct::new( "DOCUMENT_ROOT",               &zHome ),
    CgienvStruct::new( "HTTP_ACCEPT",                 &zAccept ),
    CgienvStruct::new( "HTTP_ACCEPT_ENCODING",        &zAcceptEncoding ),
    CgienvStruct::new( "HTTP_COOKIE",                 &zCookie ),
    CgienvStruct::new( "HTTP_HOST",                   &zHttpHost ),
    CgienvStruct::new( "HTTP_IF_MODIFIED_SINCE",      &zIfModifiedSince ),
    CgienvStruct::new( "HTTP_IF_NONE_MATCH",          &zIfNoneMatch ),
    CgienvStruct::new( "HTTP_REFERER",                &zReferer ),
    CgienvStruct::new( "HTTP_USER_AGENT",             &zAgent ),
    CgienvStruct::new( "PATH",                        &default_path ),
    CgienvStruct::new( "PATH_INFO",                   &zPathInfo ),
    CgienvStruct::new( "QUERY_STRING",                &zQueryString ),
    CgienvStruct::new( "REMOTE_ADDR",                 &zRemoteAddr ),
    CgienvStruct::new( "REQUEST_METHOD",              &zMethod ),
    CgienvStruct::new( "REQUEST_URI",                 &zScript ),
    CgienvStruct::new( "REMOTE_USER",                 &zRemoteUser ),
    CgienvStruct::new( "SCGI",                        &zScgi ),
    CgienvStruct::new( "SCRIPT_DIRECTORY",            &zDir ),
    CgienvStruct::new( "SCRIPT_FILENAME",             &zFile ),
    CgienvStruct::new( "SCRIPT_NAME",                 &zRealScript ),
    CgienvStruct::new( "SERVER_NAME",                 &zServerName ),
    CgienvStruct::new( "SERVER_PORT",                 &zServerPort ),
    CgienvStruct::new( "SERVER_PROTOCOL",             &zProtocol ),
];
pub fn all_test() {
    assert_eq!(&escape("a\"b\"c"), "a\"\"b\"\"c");
    println!("[INFO] ALL CHECK HAS BEEN DONE!");
}
pub fn main() {
    all_test();


}
