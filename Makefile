all: althttpd-c althttpd-rs

althttpd-c:	althttpd.c
	cc -Os -Wall -Wextra -o althttpd-c althttpd.c

althttpd-rs: althttpd.rs
	rustc althttpd.rs -o althttpd-rs

clean:	
	rm -f althttpd-c
	rm -f althttpd-rs